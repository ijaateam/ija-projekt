package ija;
import ija.gui.MazeFrame;
import ija.gui.StartFrame;

import javax.swing.*;
import java.awt.EventQueue;
import java.io.Serializable;

/**
 * Created by PeterNagy on 24/04/15.
 */
public class Labyrint implements Serializable  {
    
    // Create frames
    private static StartFrame start;
    private static MazeFrame maze;

    public static void main(String[] args) {

        Labyrint labyrint = new Labyrint();
        labyrint.createUI();
    }


    /**
     *  Creates user interface.
     *
     */
    private static void createUI(){
        
        maze = new MazeFrame();
        start = new StartFrame(maze);
        start.setVisible(true);
    }


    /**
     *  Starts NewGame after player won game.
     *
     */
    public static void startNewGame(){

        maze.setVisible(false);

        start = null;
        maze = null;

        String[] args = new String[2];

        main(args);
    }

}
