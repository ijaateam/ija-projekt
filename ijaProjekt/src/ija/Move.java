package ija;

import ija.Game.EndGame;
import ija.board.MazeBoard;
import ija.board.MazeCard;
import ija.board.MazeField;
import ija.player.Player;
import ija.treasure.GenerateTreasure;

import java.io.Serializable;

/**
 * Created by PeterNagy on 05/05/15.
 */
public class Move implements Serializable {

    private Player player;
    private int posX, posY;
    private int mazeSize;
    private MazeBoard maze;

    public Move(int playerNum, int mazeSize, MazeBoard maze, Player player){

        this.mazeSize = mazeSize;

        this.maze = maze;

        this.posX = player.getPositionX();
        this.posY = player.getPositionY();
        this.player = player;

    }


    /**
     *  Player move down on MazeBoard.
     *
     */
    public void Down(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posX < mazeSize-1){

            MazeField actualField = maze.get(posX+1, posY+1);
            MazeCard actualCard = actualField.getCard();

            MazeField nextField = maze.get(posX+2, posY+1);
            MazeCard nextCard = nextField.getCard();

            if(actualCard.canGo(MazeCard.CANGO.DOWN) && nextCard.canGo(MazeCard.CANGO.UP)){
                player.setPlayerPosition(posX+1,posY);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();
            }
        }
    }

    /**
     *  Player move up on MazeBoard.
     *
     */
    public void Up(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posX > 0){

            MazeField actualField = maze.get(posX+1, posY+1);
            MazeCard actualCard = actualField.getCard();

            MazeField nextField = maze.get(posX, posY+1);
            MazeCard nextCard = nextField.getCard();

            if(actualCard.canGo(MazeCard.CANGO.UP) && nextCard.canGo(MazeCard.CANGO.DOWN)){
                player.setPlayerPosition(posX-1,posY);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();
            }


        }

    }

    /**
     *  Player move right on MazeBoard.
     *
     */
    public void Right(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posY < mazeSize-1){

            MazeField actualField = maze.get(posX+1, posY+1);
            MazeCard actualCard = actualField.getCard();

            MazeField nextField = maze.get(posX+1, posY+2);
            MazeCard nextCard = nextField.getCard();

            if(actualCard.canGo(MazeCard.CANGO.RIGHT) && nextCard.canGo(MazeCard.CANGO.LEFT)){
                player.setPlayerPosition(posX,posY+1);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();
            }


        }

    }

    /**
     *  Player move left on MazeBoard.
     *
     */
    public void Left(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posY > 0){

            MazeField actualField = maze.get(posX+1, posY+1);
            MazeCard actualCard = actualField.getCard();

            MazeField nextField = maze.get(posX+1, posY);
            MazeCard nextCard = nextField.getCard();

            if(actualCard.canGo(MazeCard.CANGO.LEFT) && nextCard.canGo(MazeCard.CANGO.RIGHT)){
                player.setPlayerPosition(posX,posY-1);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();
            }


        }

    }


    /**
     *  Player move down after shift on MazeBoard.
     *
     */
    public void DownShift(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posX < mazeSize-1){

                player.setPlayerPosition(posX+1,posY);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();


        }
        else{
            player.setPlayerPosition(0,posY);

            this.posX = player.getPositionX();
            this.posY = player.getPositionY();
        }

    }

    /**
     *  Player move up after shift on MazeBoard.
     *
     */
    public void UpShift(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posX > 0){

                player.setPlayerPosition(posX-1,posY);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();
        }
        else{
            player.setPlayerPosition(mazeSize-1,posY);

            this.posX = player.getPositionX();
            this.posY = player.getPositionY();
        }

    }

    /**
     *  Player move right after shift on MazeBoard.
     *
     */
    public void RightShift(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posY < mazeSize-1){

                player.setPlayerPosition(posX,posY+1);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();

        }
        else{
            player.setPlayerPosition(posX,0);

            this.posX = player.getPositionX();
            this.posY = player.getPositionY();
        }

    }

    /**
     *  Player move left after shift on MazeBoard.
     *
     */
    public void LeftShift(){

        System.out.println("PositionX: "+posX+" PositionY: "+posY);

        if(posY > 0){

                player.setPlayerPosition(posX,posY-1);

                this.posX = player.getPositionX();
                this.posY = player.getPositionY();

        }
        else{
            player.setPlayerPosition(posX,mazeSize-1);

            this.posX = player.getPositionX();
            this.posY = player.getPositionY();
        }

    }


    /**
     *  Checks if player found treasure on MazeBoard.
     *
     */
    public int FoundTreasureCheck(){

        try{

            if (player.getTreasureToCollect() == maze.get(posX+1,posY+1).getCard().getMyTreasure().getTreasureCode()){

                int collected = player.getTreasureToCollect();

                maze.get(posX+1,posY+1).getCard().setMyTreasure(null);

                player.setPlayerCollectedTreasures(player.getPlayerCollectedTreasures() + 1);

                GenerateTreasure.generateNextTreasureCardForPlayer(player);

                player.setTreasureRemainsToCollect(player.getTreasureRemainsToCollect()-1);
                if(player.getTreasureRemainsToCollect() == 0){
                    EndGame end = new EndGame();
                    end.GameWinner(player.getSeqNum());
                }

                System.out.println("Treasure found, remairing: "+player.getTreasureRemainsToCollect());

                return collected;
            }

        }
        catch (NullPointerException e){
            return -1;
        }
        return -1;

    }

}
