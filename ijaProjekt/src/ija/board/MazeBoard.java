/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.board;

import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author PeterNagy
 */
public class MazeBoard implements Serializable {
    
    private MazeField[][] mazeBoard;
    private MazeCard free;
    private int numberOfFields;
    
    private static String[] type = {"C","F","L"};

    
    private MazeBoard(int n){
        mazeBoard = new MazeField[n+1][n+1];
        this.numberOfFields = n;
    }
    
    public static MazeBoard createMazeBoard(int n){
        
        MazeBoard maze = new MazeBoard(n);
        
        for(int i=1;i <= n; i++){
            for(int j=1;j <= n; j++){
                
                MazeField m = new MazeField(i, j);
                maze.mazeBoard[i][j] = m;
            }
        }
      return maze;
    }

    /**
     *  Gnerate newGame and creates MazeCards.
     *
     */
    public void newGame(){

        String randomType;
        MazeCard card;

        for(int i=1;i <= numberOfFields; i++){
            for(int j=1;j <= numberOfFields; j++){

                if(i == 1 && j == 1){
                    randomType = "C";
                    card = MazeCard.create(randomType);
                    card.turnRight();
                    card.turnRight();
                }
                else if(i == 1 && j == numberOfFields){
                    randomType = "C";
                    card = MazeCard.create(randomType);
                    card.turnRight();
                    card.turnRight();
                    card.turnRight();
                }
                else if(i == numberOfFields && j == 1){
                    randomType = "C";
                    card = MazeCard.create(randomType);
                    card.turnRight();
                }
                else if(i == numberOfFields && j == numberOfFields){
                    randomType = "C";
                    card = MazeCard.create(randomType);
                }
                else if((i%2) == 1 && (j%2) == 1){
                    randomType = "F";
                    card = MazeCard.create(randomType);

                    if(j == 1){
                        card.turnRight();
                    }
                    else if(i == 1){
                        card.turnRight();
                        card.turnRight();
                    }
                    else if(j == numberOfFields){
                        card.turnRight();
                        card.turnRight();
                        card.turnRight();
                    }
                    else if(i == numberOfFields);
                    else{
                        int randNum = randInt();
                        for (int k = 0; k < randNum; ++k) {
                            card.turnRight();
                        }
                    }
                }
                else {
                    randomType = (type[new Random().nextInt(type.length)]);
                    card = MazeCard.create(randomType);

                    int randNum = randInt();
                    //System.out.println("Random number: " + randNum + " Type: " + randomType);
                    for (int k = 0; k < randNum; ++k) {
                        card.turnRight();
                    }
                }

                mazeBoard[i][j].putCard(card);
            }
        }
        randomType = (type[new Random().nextInt(type.length)]);
        card = MazeCard.create(randomType);
        free = card;
    }

    /**
     *  Gets free card.
     *
     */
    public MazeCard getFreeCard(){
        if(free != null)
            return free;
        return null;
    }

    /**
     *  Gets MazeField on MazeBoad on specified position.
     *
     *  @param r Row number.
     *  @param c Col number.
     *
     */
    public MazeField get(int r, int c){
        //System.out.println("R: "+r+"C: "+c);
        if(mazeBoard[r][c] != null)
            return mazeBoard[r][c];
        return null;
    }

    /**
     *  Shift cards on MazeBoard.
     *
     */
    public void shift(MazeField mf){
        
        int c = mf.col();
        int r = mf.row();
        MazeField tmp = new MazeField(r, c);
        
        if((r == 1) && (c%2 == 0)){
            
            tmp.putCard(mazeBoard[numberOfFields][c].getCard());
            
            for(int i=numberOfFields;i>1 ; i--){
                mazeBoard[i][c].putCoordinates(i-1, c);
                mazeBoard[i][c].putCard(mazeBoard[i-1][c].getCard());
                mazeBoard[i][c].putRockChar(mazeBoard[i-1][c].getCard().getRock());
            }
            mazeBoard[1][c].putCard(free);
            free = tmp.getCard();
        }
        
        if((r == numberOfFields) && (c%2 == 0)){
            
            tmp.putCard(mazeBoard[1][c].getCard());
            
            for(int i=1; i<numberOfFields; i++){
                mazeBoard[i][c].putCoordinates(i+1, c);
                mazeBoard[i][c].putCard(mazeBoard[i+1][c].getCard());
                mazeBoard[i][c].putRockChar(mazeBoard[i+1][c].getCard().getRock());
            }
            mazeBoard[numberOfFields][c].putCard(free);
            free = tmp.getCard();
        }
        
        if((c == 1) && (r%2 == 0)){
            tmp.putCard(mazeBoard[r][numberOfFields].getCard());
            
            for(int i=numberOfFields;i>1;i--){
                mazeBoard[r][i].putCoordinates(r, i-1);
                mazeBoard[r][i].putCard(mazeBoard[r][i-1].getCard());
                mazeBoard[r][i].putRockChar(mazeBoard[r][i-1].getCard().getRock());
            }
            mazeBoard[r][1].putCard(free);
            free = tmp.getCard();
        }
        
        if((c == numberOfFields) && (r%2 == 0)){
            tmp.putCard(mazeBoard[r][1].getCard());
            
            for(int i=1;i<numberOfFields;i++){
                mazeBoard[r][i].putCoordinates(r, i+1);
                mazeBoard[r][i].putCard(mazeBoard[r][i+1].getCard());
                mazeBoard[r][i].putRockChar(mazeBoard[r][i+1].getCard().getRock());
            }
            mazeBoard[r][numberOfFields].putCard(free);
            free = tmp.getCard();
        }
        
    }
    
    public MazeField getField(int r, int c){
        return mazeBoard[r][c];
    }

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    private static int randInt() {

        int min = 0;
        int max = 3;
        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }




}
