/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.board;

import java.io.Serializable;

/**
 *
 * @author PeterNagy
 */
public class MazeField implements Serializable {
    
    private int row;
    private int col;
    
    private MazeCard rock = null;
    
    public MazeField(int row, int col){
        this.row = row;
        this.col = col;
    }

    /**
     *  Gets row.
     *
     *  @return row position.
     */
    public int row(){
        return row;
    }

    /**
     *  Gets col.
     *
     *  @return col position.
     */
    public int col(){
        return col;
    }

    /**
     *  Gets card on MazeField.
     *
     *  @return row position.
     */
    public MazeCard getCard(){
        if(rock != null){
            return rock;
        }
        return null;
    }

    /**
     *  Puts card to mazeField.
     *
     */
    public void putCard(MazeCard c){
        rock = c;
    }

    /**
     *  Puts coordinates to MazeField.
     *
     */
    public void putCoordinates(int row, int col){
        this.col = col;
        this.row = row;  
    }

    /**
     *  Puts rock to MazeCard on MazeField.
     *
     */
    public void putRockChar(String rockChar){
        if(rock != null){
            rock.putRock(rockChar);
        }
    }
    
    
}
