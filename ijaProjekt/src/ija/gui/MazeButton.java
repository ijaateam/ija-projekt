
package ija.gui;

import java.awt.*;
import java.io.Serializable;
import javax.swing.*;

/**
 *
 * @author PeterNagy
 */
public class MazeButton extends JButton implements Serializable {
    private ImageIcon C,L,F;
    transient private Image img, newimg;
    private String type;


    /**
     *  Mazebutton represents button on the maze
     *
     *  @param type   button type
     */
    public MazeButton(String type){

        this.type = type;

        // to remote the spacing between the image and button's borders
        this.setMargin(new Insets(0, 0, 0, 0));
        // to remove the border
        this.setBorder(null);

        this.setFocusable(false);

    }


    /**
     *  Show button image on mazeboard.
     *
     *  @param rotation   the angle of rotation
     *  @param size board size
     */
    public void showButtonImage(int rotation, int size){

        switch(type){
            case "C":

                C = new ImageIcon(this.getClass().getResource("L.png"));
                img = C.getImage();
                newimg = img.getScaledInstance(size, size, java.awt.Image.SCALE_SMOOTH) ;
                C = new ImageIcon( newimg );

                setIcon(rotate(C,rotation));

                break;
            case "L":

                L = new ImageIcon(this.getClass().getResource("I.png"));
                img = L.getImage();
                newimg = img.getScaledInstance(size, size, java.awt.Image.SCALE_SMOOTH) ;
                L = new ImageIcon( newimg );
                setIcon(rotate(L,rotation));
                break;
            case "F":

                F = new ImageIcon(this.getClass().getResource("T.png"));
                img = F.getImage();
                newimg = img.getScaledInstance( size, size,  java.awt.Image.SCALE_SMOOTH ) ;
                F = new ImageIcon( newimg );
                setIcon(rotate(F,rotation));
                break;

        }
    }


    /**
     *  Rotate image icon.
     *
     *  @param img    Image to rotate
     *  @param rotation   the angle of rotation
     *
     *  @return rotated icon
     */
    private RotatedIcon rotate(ImageIcon img, int rotation){

        RotatedIcon ri;

        if(rotation == 90){
            ri = new RotatedIcon(img, RotatedIcon.Rotate.DOWN);
        }
        else if(rotation == 180){
            ri = new RotatedIcon(img, RotatedIcon.Rotate.UPSIDE_DOWN);
        }
        else if(rotation == 270){
            ri = new RotatedIcon(img, RotatedIcon.Rotate.UP);
        }
        else{
            ri = new RotatedIcon(img, RotatedIcon.Rotate.ABOUT_CENTER);
        }

        return ri;
    }

    public void setType(String type){
        this.type = type;
    }


    
}
