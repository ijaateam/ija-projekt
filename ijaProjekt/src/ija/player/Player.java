package ija.player;

import ija.gui.MazeFrame;

import java.io.Serializable;

/**
 * Created by PeterNagy on 24/04/15.
 */
public class Player implements Serializable {

//    public static List<Player> playerList = new ArrayList<>();
    
    private String name;
    private int positionX;
    private int positionY;
    private int seqNum;
    transient private MazeFrame maze;
    public int playerCount;
    private int playerCollectedTreasures;
    private int treasureToCollect;
    private int treasureRemainsToCollect;
    
    public Player(int n, int positionX, int positionY, int seqNum, MazeFrame maze, int treasureRemainsToCollect){
        this.name = "Player" + n;
        this.positionX = positionX;
        this.positionY = positionY;
        this.seqNum = seqNum;
        this.maze = maze;
        this.playerCount = n;
        this.playerCollectedTreasures = 0;
        this.treasureRemainsToCollect = treasureRemainsToCollect;
    }

    public int getSeqNum(){
        return seqNum;
    }

    public int getPositionX(){
        return positionX;
    }

    public int getPositionY(){
        return positionY;
    }

    public int getPlayerCount(){
        return playerCount;
    }

    public void setPlayerPosition(int x, int y){

        positionX = x;
        positionY = y;

        System.out.println("SET PLAYER POSITION: "+x+"Y: "+y);

        if(seqNum == 0){
            maze.setPlayer1Image(positionX,positionY);
        }
        else if(seqNum == 1){
            maze.setPlayer2Image(positionX, positionY);
        }
        else if(seqNum == 2){
            maze.setPlayer3Image(positionX, positionY);
        }
        else if(seqNum == 3){
            maze.setPlayer4Image(positionX, positionY);
        }

        maze.repaint();
    }

    public void setTreasureToCollect(int treasureToCollect){
        this.treasureToCollect = treasureToCollect;
    }

    public int getTreasureToCollect(){
        return treasureToCollect;
    }


    // Number of collected treasures by player
    public void setPlayerCollectedTreasures(int playerCollectedTreasures){
        this.playerCollectedTreasures = playerCollectedTreasures;
    }
    public int getPlayerCollectedTreasures(){
        return playerCollectedTreasures;
    }

    // Treasures remairing to collect
    public void setTreasureRemainsToCollect(int treasureRemainsToCollect){
        this.treasureRemainsToCollect = treasureRemainsToCollect;
    }
    public int getTreasureRemainsToCollect(){
        return treasureRemainsToCollect;
    }

    public void setMaze(MazeFrame maze){
        this.maze = maze;
    }

}
