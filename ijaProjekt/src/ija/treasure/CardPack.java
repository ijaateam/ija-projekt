/*
 * Pack of cards
 */
package ija.treasure;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author PeterNagy
 */
public class CardPack implements Serializable {
    
    private int maxSize;
    private int initSize;
    
    private Queue<TreasureCard> pack = new LinkedList<>();
    
    public CardPack(int maxSize, int initSize){
        
        if(maxSize >= initSize){
            this.maxSize = maxSize;
            this.initSize = initSize;
            
            //Treasure.createSet();
            for(int i=0; i<maxSize;i++){
                TreasureCard tr = new TreasureCard(Treasure.getTreasure(i));
                
                pack.add(tr);
            }
        }   
    }

    /**
     *  Get first card from queue.
     *
     *  @return gets card from queue of cards.
     */
    public TreasureCard popCard(){
        
        TreasureCard card = pack.poll();
        return card;
    }

    /**
     *  Get size of card pack.
     *
     *  @return card pack size.
     */
    public int size(){
        return pack.size();
    }

    /**
     *  Shuffle cards in pack.
     *
     */
    public void shuffle(){
        Collections.shuffle((List<?>) pack, new Random());
    }
    
    
}
