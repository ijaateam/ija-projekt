package ija.Game;

import ija.Labyrint;

import javax.swing.*;
import java.io.Serializable;

/**
 * @author Peter Nagy.
 */
public class EndGame extends JOptionPane implements Serializable {


    /**
     *  Called on the end of the game.
     *
     *  @param playerNum    Player ID.
     *
     */
    public void GameWinner(int playerNum){

        JOptionPane.showMessageDialog(this, "Congratulation you win the game! Player "+(playerNum+1)+".", null, JOptionPane.INFORMATION_MESSAGE, null);

        Labyrint.startNewGame();
    }

}
