/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.SaveLoad;

import ija.Move;
import ija.board.MazeBoard;
import ija.gui.*;
import ija.player.Player;
import ija.treasure.CardPack;
import java.io.FileOutputStream;
import java.io.*;
import java.io.ObjectOutputStream;
import javax.swing.JPanel;

/**
 *
 * @author danielharis
 */
public final class SaveGame implements Serializable {
    
    // Buttons array
    private MazeButton[][] buttonArray; // ++
    // Out buttons
    private OutButton[][] outButtons; 
    // Panel array for each button - Player position
    private JPanel[][] panelArray;
    // MazeBoard stored
    MazeBoard myMazeBoard;
    // Was Shifted in this move
    private Boolean wasShifted = false;
    // Stored maze size
    private int mazeSize = 0;
    javax.swing.JLabel player1Label = new javax.swing.JLabel("");
    javax.swing.JLabel player2Label = new javax.swing.JLabel("");
    javax.swing.JLabel player3Label = new javax.swing.JLabel("");
    javax.swing.JLabel player4Label = new javax.swing.JLabel("");
    // Treasure label array
    javax.swing.JLabel[] treasureLabel;
    // One move
    private Move[] move;
    private int playerOnTheMove = 0;
    private int playerCount = 2;
    Player[] playerList;
    CardPack pack;
    String[][] buttonType;
    int treasureCount;

    
    // konstruktor
    public SaveGame(MazeButton[][] buttonArray, OutButton[][] outButtons, JPanel[][] panelArray,
            MazeBoard myMazeBoard, Boolean wasShifted, int mazeSize, javax.swing.JLabel player1Label, 
            javax.swing.JLabel player2Label, javax.swing.JLabel player3Label, javax.swing.JLabel player4Label, 
            javax.swing.JLabel[] treasureLabel, Move[] move, int playerOnTheMove, int playerCount, Player[] playerList,
            CardPack pack, String[][] buttonType, int treasureCount)
    {
        this.mazeSize = mazeSize;
        this.myMazeBoard = myMazeBoard;
        this.wasShifted = wasShifted;
        this.player1Label = player1Label;
        this.player2Label = player2Label;
        this.player3Label = player3Label;
        this.player4Label = player4Label;
        this.playerOnTheMove = playerOnTheMove;
        this.playerCount = playerCount;
        setButtonArray(buttonArray);
        setOutButton(outButtons);
        setPannelArray(panelArray);
        setTreasureLabel(treasureLabel);
        setMove(move);
        setPlayerList(playerList);
        this.pack = pack;
        setButtonType(buttonType);
        this.treasureCount = treasureCount;
    }
    
    public void setButtonArray(MazeButton[][] buttonArray){
        this.buttonArray = new MazeButton[buttonArray.length][];
        for(int i = 0; i < buttonArray.length; i++){
            this.buttonArray[i] = buttonArray[i].clone();
        }
    }
    
    // mozno nebude treba
    public void setOutButton(OutButton[][] outButtons){
        this.outButtons = new OutButton[outButtons.length][];
        for(int i = 0; i < outButtons.length; i++){
            this.outButtons[i] = outButtons[i].clone();
        }
    }
    
    public void setPannelArray(JPanel[][] panelArray){
        this.panelArray = new JPanel[panelArray.length][];
        for(int i = 0; i < panelArray.length; i++){
            this.panelArray[i] = panelArray[i].clone();
        }
    }

    public void setButtonType(String[][] buttonType){
        this.buttonType = new String[buttonType.length][];
        for(int i = 0; i < panelArray.length; i++){
            this.buttonType[i] = buttonType[i].clone();
        }
    }

    public void setTreasureLabel(javax.swing.JLabel[] treasureLabel){
        this.treasureLabel = new javax.swing.JLabel[treasureLabel.length];
        System.arraycopy(treasureLabel, 0, this.treasureLabel, 0, treasureLabel.length);
    }

    public void setMove(Move[] move){
        this.move = new Move[move.length];
        System.arraycopy(move, 0, this.move, 0, move.length);
    }

    public void setPlayerList(Player[] playerList){
        this.playerList = new Player[playerList.length];
        System.arraycopy(playerList, 0, this.playerList, 0, playerList.length);
    }

    public void saveGame(File file){
        
        try(FileOutputStream saveFile = new FileOutputStream(file)){

            // Create an ObjectOutputStream to put objects into save file.
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
//             Now we do the save. sorted lexicographically
            //save.writeObject(this.buttonArray);
            save.writeObject(this.mazeSize);
            save.writeObject(this.move);
            save.writeObject(this.myMazeBoard);
            save.writeObject(this.panelArray);
            save.writeObject(this.player1Label);
            save.writeObject(this.player2Label);
            save.writeObject(this.player3Label);
            save.writeObject(this.player4Label);
            save.writeObject(this.playerCount);
            save.writeObject(this.playerOnTheMove);
            save.writeObject(this.treasureLabel);
            save.writeObject(this.wasShifted);
            save.writeObject(playerList);
            save.writeObject(this.pack);
            save.writeObject(this.buttonType);
            save.writeObject(this.treasureCount);

            save.close();
            
        } catch (FileNotFoundException e) {
            e.printStackTrace(); // If there was an error, print the info.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
