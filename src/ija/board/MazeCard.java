/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ija.board;

import ija.treasure.Treasure;

import java.io.Serializable;

/**
 *
 * @author PeterNagy
 */
public class MazeCard implements Serializable {

    
    private MazeCard(CANGO left, CANGO right, CANGO up, CANGO down, String rockType){
        this.left = left;
        this.right = right;
        this.up = up;
        this.down = down;
        
        this.rockType = rockType;
    }
    
    public static enum CANGO{LEFT, UP, RIGHT, DOWN};
    
    private CANGO left;
    private CANGO right;
    private CANGO up;
    private CANGO down;
    
    private String rockType;

    private int cardTreasureCode = -1;  // Default - no treasure on card


    private Treasure myTreasure;

    /**
     *  Create MazeCards.
     *
     *  @param type   MazeCard type {C,L,F}
     *
     *  @return created MazeCard.
     */
    public static MazeCard create(String type){
        MazeCard maze;
        switch(type){
            
            case "C":
                maze = new MazeCard(CANGO.LEFT, null, CANGO.UP, null, "C");
                break;
            case "L":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, null, null, "L");
                break;
            case "F":
                maze = new MazeCard(CANGO.LEFT, CANGO.RIGHT, CANGO.UP, null, "F");
                break;
            default:
                throw new IllegalArgumentException();
        }
        
        return maze;
    }

    /**
     *  Cango to specified side.
     *
     *  @param dir   direction to go.
     *
     *  @return bool value if player can go to specifies side
     */
    public boolean canGo(MazeCard.CANGO dir){
        
        if(dir.equals(CANGO.LEFT)){
            if(left != null)
                return true;
        }
        if(dir.equals(CANGO.RIGHT)){
            if(right != null)
                return true;
        }
        if(dir.equals(CANGO.UP)){
            if(up != null)
                return true;
        }
        if(dir.equals(CANGO.DOWN)){
            if(down != null)
                return true;
        }
        
        return false;
    }

    /**
     *  Turns right MazeBoard.
     *
     */
    public void turnRight(){
        
        CANGO tmp;
        tmp = up;
        up = left;
        left = down;
        down = right;
        right = tmp;
        
        if(up != null)
            up = CANGO.UP;
        if(down != null)
            down = CANGO.DOWN;
        if(left != null)
            left = CANGO.LEFT;
        if(right != null)
            right = CANGO.RIGHT;
        
    }

    /**
     *  Gets rock on MazeCard.
     *
     *  @return type of rock on MazeCard.
     */
    public String getRock(){
        return rockType;
    }

    /**
     *  Puts rock on MazeCard.
     *
     *  @param rockChar rock to put.
     *
     */
    public void putRock(String rockChar){
        rockType = rockChar;
    }

    /**
     *  Sets treasure on MazeCard.
     *
     *  @param myTreasure treasure to set.
     *
     */
    public void setMyTreasure(Treasure myTreasure){
        this.myTreasure = myTreasure;
    }

    /**
     *  Gets treasure on MazeCard.
     *
     *  @return actual treasure on MazeCard.
     */
    public Treasure getMyTreasure(){
        return myTreasure;
    }
    
}
