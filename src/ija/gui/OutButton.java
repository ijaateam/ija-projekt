package ija.gui;


import ija.Move;
import ija.board.MazeBoard;
import ija.board.MazeField;
import ija.player.Player;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

/**
 * Created by PeterNagy on 06/05/15.
 */
public class OutButton extends JButton implements ActionListener, Serializable {

    transient private int col;
    transient private int row;
    transient private MazeBoard maze;
    transient private MazeFrame frame;
    transient private static Move move[];

    transient private static int lastShiftCol = -1;
    transient private static int lastShiftRow = -1;
    transient private static int shiftType = -1;




    public OutButton(int row, int col, MazeBoard maze, MazeFrame frame){
        this.col = col;
        this.row = row;
        this.addActionListener(this);
        this.maze = maze;
        this.frame = frame;
    }

    public void actionPerformed(ActionEvent e){

        // There was no shift in this move
        if(frame.getWasShifted() == false){


            // Reverse shift not allowed
            if (row == lastShiftRow && col != lastShiftCol && shiftType == 0){
                return;
            }

            else if(col == lastShiftCol && row != lastShiftRow && shiftType == 1)
                return;

            lastShiftRow = row;
            lastShiftCol = col;

            if (row == 1 || row == StartFrame.getBoardSize())
                shiftType = 1;
            if (col == 1 || col == StartFrame.getBoardSize())
                shiftType = 0;



            for(int i=0;i<frame.getPlayerCount();i++){
                Player myPlayer =  frame.getStaticPlayer(i);

                if (row == 1 && myPlayer.getPositionY() == col-1){
                    move[i].DownShift();
                }
                else if (row == StartFrame.getBoardSize() && myPlayer.getPositionY() == col-1){
                    move[i].UpShift();
                }
                else if (col == 1 && myPlayer.getPositionX() == row-1){
                    move[i].RightShift();
                }
                else if (col == StartFrame.getBoardSize() && myPlayer.getPositionX() == row-1){
                    move[i].LeftShift();
                }

            }

            MazeField field = new MazeField(row, col);

            System.out.println("ROW: "+row);
            System.out.println("COL: "+col);


            // Shift
            maze.shift(field);
            frame.setCanMove(true);
            frame.repaintNow(maze);


        }


    }


    public static void setMove(Move[] myMove){
        move = myMove;

    }



}
