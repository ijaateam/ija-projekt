package ija.treasure;

import ija.board.MazeBoard;
import ija.board.MazeField;
import ija.gui.MazeFrame;
import ija.gui.StartFrame;
import ija.player.Player;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by PeterNagy on 06/05/15.
 */
public class GenerateTreasure implements Serializable {

    private int treasureCount;
    private MazeBoard maze;
    private MazeFrame frame;
    private static CardPack pack;
    private int boardSize;

    private static int[][] treasureIconPosition;

    private static ArrayList<Integer> treasureNum = new ArrayList<Integer>();

    public GenerateTreasure(int treasureCount, MazeBoard maze, MazeFrame frame, CardPack pack, int boardSize){

        this.treasureCount = treasureCount;
        this.maze = maze;
        this.frame = frame;
        this.pack = pack;
        this.boardSize = boardSize;

        treasureIconPosition = new int[boardSize][boardSize];

        for (int i=0;i<boardSize;i++){
            for (int j=0;j<boardSize;j++){
                treasureIconPosition[i][j] = -1;
            }
        }

    }

    /**
     *  Sets treasure to MazeBoard.
     *
     *  @param myMazeBoard MazeBoard to set treasure.
     *
     */
    public void setTreasureToBoard(MazeBoard myMazeBoard){

        pack.shuffle();

        int x;
        int y;

        uniqueRandNum(treasureCount);

        for (int i=0;i<treasureCount;){

            x = randInt(1,boardSize);
            y = randInt(1,boardSize);

            // No corners
            if (x == 1 && y == 1)
                continue;
            if (x == 1 && y== StartFrame.getBoardSize())
                continue;
            if (x == StartFrame.getBoardSize() && y == 1)
                continue;
            if (x == StartFrame.getBoardSize() && y == StartFrame.getBoardSize())
                continue;


            if(treasureIconPosition[x-1][y-1] != -1) {
                continue;
            }
            else{


                    //MazeButton myMazeButton = frame.getMazeButton(x, y);
                    int code = treasureNum.get(i);
                    Treasure myTreasure = Treasure.getTreasure(code);
                    myTreasure.setTreasureCode(code);

                    MazeField myField = myMazeBoard.get(x,y);
                    if(myField == null){
                        continue;
                    }
                    myField.getCard().setMyTreasure(myTreasure);

                    treasureIconPosition[x-1][y-1] = treasureNum.get(i);

                frame.setTreasureImage(x,y);
                frame.setTreasure(x, y);

                    i++;

            }
        }

    }

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }



    /**
     *  Generate unique random number.
     *
     *  @param numOfRand number of random numbers.
     *
     */
    public static void uniqueRandNum(int numOfRand) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i=0; i<numOfRand; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i=0; i<numOfRand; i++) {
            treasureNum.add(list.get(i));

        }
    }


    /**
     *  Generating treasure card for player.
     *
     * @param numberOfPlayers number of players.
     *
     */
    public void generateTreasureCardForPlayer(int numberOfPlayers){

        for (int i=0; i<numberOfPlayers; i++){

            Player actualPlayer = frame.getStaticPlayer(i);

            TreasureCard myTreasureCard = pack.popCard();

            actualPlayer.setTreasureToCollect(myTreasureCard.getCode());

        }

    }


    /**
     *  Generate next TreasureCard for player.
     *
     *  @param actualPlayer actual player to generate treasure.
     *
     */
    public static void generateNextTreasureCardForPlayer(Player actualPlayer){


        TreasureCard myTreasureCard = pack.popCard();

        actualPlayer.setTreasureToCollect(myTreasureCard.getCode());

    }


}
