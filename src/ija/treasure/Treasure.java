/*
 * Treasure
 */
package ija.treasure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author PeterNagy
 */
public class Treasure implements Serializable {
    
    static private int treasureNum = 24;
    private int code;
//    private ImageIcon image;
    static List<Treasure> treasure = new ArrayList<>();

    private int treasureCode;

    /**
     *  Get treasure code.
     *
     *  @return treasure code.
     */
    public int getCode(){
        return code;
    }
    
    // Constructor
    private Treasure(int code){
        this.code = code;
    }


    /**
     *  Create set of treasures.
     *
     *  @param n number of treasures to create.
     *
     */
    public static void createSet(int n){
        treasureNum = n;

        // Creating objects
        for(int i=0; i<treasureNum; i++){
            Treasure t = new Treasure(i);
            treasure.add(t);
        }
        
    }


    /**
     *  Get treasure from list of treasures.
     *
     *  @param code Code of treasure.
     *
     *  @return treasure.
     */
    public static Treasure getTreasure(int code){
        
        for(Treasure t : treasure){
            if(t.code == code){
                return t;
            }
        }      
        return null;
    }

    /**
     *  Get treasure code.
     *
     *  @return treasure code.
     */
    public int getTreasureCode(){
        return treasureCode;
    }

    /**
     *  Set treasure code.
     *
     *  @param treasureCode code of treasure.
     *
     */
    public void setTreasureCode(int treasureCode){
        this.treasureCode = treasureCode;
    }

    
    
}
