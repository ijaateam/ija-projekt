/*
 * Threasure card
 */
package ija.treasure;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author PeterNagy
 */
public class TreasureCard implements Serializable {
    
    private Treasure tr;
    
    public TreasureCard(Treasure tr){
        this.tr = tr;
    }

    @Override
    public int hashCode() {
        return tr.getCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TreasureCard other = (TreasureCard) obj;
        if (!Objects.equals(this.tr, other.tr)) {
            return false;
        }
        return true;
    }


    /**
     *  Get treasure code.
     *
     *  @return treasure code.
     */
    public int getCode(){
        return tr.getCode();
    }

}
